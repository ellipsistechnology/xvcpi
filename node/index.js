const addon = require('bindings')('xvcpi'); // import 'xvcpi.node'
const fs = require('fs')

let port = 2542

const STATE_FILE = '/opt/blackbox/state.json'

exports.start = function(state) {
    if(state) {
        fs.writeFile(STATE_FILE, JSON.stringify(state, null, 2), 'utf8', () => console.log(`state written to ${STATE_FILE}`))
    } else {
        console.error('state was not given when starting xvcpi');
    }
    return new Promise((resolve, reject) => {
        // Randomise port:
        port = 2500 + Math.floor(Math.random()*1000) // between 2500 and 3500
        try {
            addon.start(port, () => console.log)
            resolve()
        } catch(err) {
            reject(err)
        }
    })
}

function getState() {
    return fs.promises.readFile(STATE_FILE)
    .then(buffer => JSON.parse(buffer.toString()))
}
exports.stop = function() {
    getState()
    .then(state => {
        delete state.owner
        fs.writeFile(STATE_FILE, JSON.stringify(state, null, 2), 'utf8', () => console.log(`owner removed from ${STATE_FILE}`))
    })
    .catch(err => {
        console.error(err)
        fs.writeFile(STATE_FILE, '{}', 'utf8', () => console.log(`${STATE_FILE} cleared after error`))
    })
    addon.stop()
}

exports.info = ({req}) => {
    return getState()
    .then(state => {
        const info = {
            version: '0.0.1'
        }
        if(req) {
            const url = new URL('http://'+req.headers.host)
            const host = url.hostname
            const protocol = req.connection.encrypted ? 'https' : 'http'
            info.programUrl = `${protocol}://${host}:81/program?url=http%3A%2F%2F${host}%3A${port}`
            info.controlUrl = `${protocol}://${host}:81/control?url=${protocol}%3A%2F%2F${host}%3A8090`
        }
        if(state && state.owner && state.owner.username) {
            info.owner = state.owner.username
        }
        return info
    })
    
}//addon.info