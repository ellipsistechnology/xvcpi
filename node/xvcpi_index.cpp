#include "xvcpi.h"
#include <napi.h>
#include <string>

int port = 2542;

class XVCPiWorker : public Napi::AsyncWorker {
  public:
    XVCPiWorker(Napi::Function& callback)
      : Napi::AsyncWorker(callback) {}
    ~XVCPiWorker() {}

    void Execute() {
      result = start(port);
    }

    void OnOK() {
      Callback().Call({/*err:*/Env().Undefined(), Napi::Number::New(Env(), result)});
    }

  private:
    int result;
};

Napi::Value xvcpi_start(const Napi::CallbackInfo &info) {
  port = (int) info[0].ToNumber();
  Napi::Function callback = info[1].As<Napi::Function>();
  XVCPiWorker* xvcpiWorker = new XVCPiWorker(callback);
  xvcpiWorker->Queue();
  return info.Env().Undefined();
}

Napi::Value xvcpi_stop(const Napi::CallbackInfo &info) {
  stop();
  return info.Env().Undefined();
}

// Napi::Object xvcpi_info(const Napi::CallbackInfo &info) {
//   Napi::Object i = Napi::Object::New(info.Env());
//   i.Set("version", "0.0.1");
//   i.Set("controlURL", );
//   i.Set("programURL", );
//   return i;
// }

Napi::Object Init(Napi::Env env, Napi::Object exports) {
  exports.Set(
    Napi::String::New(env, "start"),
    Napi::Function::New(env, xvcpi_start)
  );
  exports.Set(
    Napi::String::New(env, "stop"),
    Napi::Function::New(env, xvcpi_stop)
  );
  // exports.Set(
  //   Napi::String::New(env, "info"),
  //   Napi::Function::New(env, xvcpi_info)
  // );
  return exports;
}

NODE_API_MODULE(xvcpi, Init)